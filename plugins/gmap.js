import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import Geocoder from '@pderas/vue2-geocoder'

const GMAP_KEY = 'AIzaSyACBJDYDUpn5m5ZBq-MbnPF8O74LCS6AeQ'

Vue.use(VueGoogleMaps, {
  load: {
    key: GMAP_KEY,
    libraries: 'places'
  }
})

Vue.use(Geocoder, {
  defaultCountryCode: null, // e.g. 'CA'
  defaultLanguage: 'th', // e.g. 'en'
  defaultMode: 'lat-lng', // or 'lat-lng'
  googleMapsApiKey: GMAP_KEY
})
