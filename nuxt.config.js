import * as EN_LANG from './lang/en.json'
import * as DE_LANG from './lang/de.json'
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'jfnetwork',
    htmlAttrs: {
      lang: 'en',
      title: 'JF Network | Unsere Spezialisierung, Ihr Vorteil, IT-Lösungen für den Automotivsektor'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Unsere Spezialisierung, Ihr Vorteil, IT-Lösungen für den Automotivsektor' },
      { hid: 'keywords', name: 'description', content: 'Unsere Spezialisierung, Ihr Vorteil, IT-Lösungen für den Automotivsektor' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon-32x32.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700,800,900' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/global.css',
    'swiper/css/swiper.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~plugins/vue-awesome-swiper', mode: 'client' },
    { src: '~plugins/gmap', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/i18n'
  ],
  i18n: {
    locales: ['en', 'de'],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: EN_LANG,
        de: DE_LANG
      }
    },
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      onlyOnRoot: true // recommended
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
