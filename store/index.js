export const state = () => ({
  pageTitle: ''
})

export const actions = {
  nuxtServerInit (store, context) {

  }
}

export const mutations = {
  SET_PAGE_TITLE (state, pageTitle) {
    state.pageTitle = pageTitle
  }
}

export const getters = {
  getPageTitle (state) {
    return state.pageTitle
  }
}